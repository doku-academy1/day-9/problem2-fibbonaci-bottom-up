import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        System.out.println(fibbonaci(5));
        System.out.println(fibbonaci(6));
        System.out.println(fibbonaci(7));
        System.out.println(fibbonaci(50));
    }

    public static Long fibbonaci(Integer number) {
        if (number <= 1) {
            return (long) number;
        }
        Long A = (long) 0;
        Long B = A + 1;
        Long total = (long) 0;
        for (Integer i = 2; i<=number; i++) {
            total = A + B;
            A = B;
            B = total;
        }
        return B;
    }
}